
import sys

def line(number: int):
    linea = ''
    for i in range(1, number + 1):
        linea += str(number)
    return linea


def triangle(number: int):
    triangulo = ''
    for i in range(1, number + 1):
         triangulo += line(i) + '\n'
    return triangulo


def main():
    number: int = sys.argv[1] if len(sys.argv) > 1 else 5
    text = triangle(int(number))
    print(text)

if __name__ == '__main__':
    main()



